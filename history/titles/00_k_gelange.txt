k_gelange = {
	220.1.1 = { change_development_level = 6 }
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

d_serchel = {
	220.1.1 = { change_development_level = 8 }
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_coerlange = {
	220.1.1 = { change_development_level = 15 }
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_aigiloe = {
	220.1.1 = { change_development_level = 10 }
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_crofourde = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_curotole = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_camtelier = {
	220.1.1 = { change_development_level = 9 }
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_sugarte = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_espereuroes = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

d_vourde = {
	220.1.1 = { change_development_level = 4 }
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_aulgetaine = {
	220.1.1 = { change_development_level = 8 }
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_vousiera = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_cibouse = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

d_vroloire = {
	220.1.1 = { change_development_level = 3 }
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_neus_vior = {
	220.1.1 = { change_development_level = 5 }
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_aigivel = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_pieria = {
	220.1.1 = { change_development_level = 2 }
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

d_aigilourda = {
	220.1.1 = { change_development_level = 4 }
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_cerloselle = {
	220.1.1 = { change_development_level = 7 }
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_noireile = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_sutores = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_voutole = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_patoira = {
	220.1.1 = { change_development_level = 5 }
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_tesor = {
	220.1.1 = { change_development_level = 5 }
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

d_teirone = {
	220.1.1 = { change_development_level = 5 }
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_caulerete = {
	220.1.1 = { change_development_level = 8 }
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_pieragile = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_bosouia = {
	220.1.1 = { change_development_level = 6 }
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_choime = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_curouaul = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_cipioran = {
	220.1.1 = { change_development_level = 6 }
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_ceisetes = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

d_meivouia = {
	220.1.1 = { change_development_level = 3 }
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_tes_mirsoen = {
	220.1.1 = { change_development_level = 6 }
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_jehansaive = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_verlalonga = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_sutuvon = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_noruvon = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_contirel = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

d_cairslaige = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_temp1a = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_temp2a = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_temp3a = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_temp4a = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_temp5a = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_temp6a = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

d_espereus = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_temp1b = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_temp2b = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_temp3b = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_temp4b = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_temp5b = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

d_maugestale = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_temp1c = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_temp2c = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_temp3c = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_temp4c = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

d_cate_ser = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_temp1d = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_temp2d = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_temp3d = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_temp4d = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_temp5d = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_temp6d = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

d_princoives = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_temp1e = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_temp2e = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_temp3e = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_temp4e = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_temp5e = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

d_cibouia = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_estavois = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_setebeset = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}

c_iocharam = {
	190.1.1 = {
		holder = 6000000
		government = feudal_government
	}
}